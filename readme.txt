=== Prayer Times Anywhere ===
Contributors: mmrs151
Donate link: http://edgwareict.org.uk/
Tags: prayer time, salah time, islam, namaz, timetable
Requires at least: 4.0
Tested up to: 4.3
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display daily salah begining time based on your current location.

== Description ==
Alhamdulillah.

This plugin allows you to display daily salah time start based on your current location.
It Uses API service from muslimsalat.com for timing and google map API to display your current location.
This APIs are used to get an approx location to display time and map.


= Features =
Once the installation above is done, this will allow you to

- display prayer start time
- display prayer time either vertical or horizontal widget.
- To chose from three different themes

= Upcoming features =
- Select your location
- select calculation method
- get daily/weekly/monthly or yearly timings
- Set Daylight saving and many more inshaaAllaah

= shortcodes =
coming soon ...

== Installation ==
1. Download the plugin
2. Simply go under the Plugins page, then click on Add new and select the plugin's .zip file
3. Alternatively you can extract the contents of the zip file directly to your wp-content/plugins/ folder
4. Finally, just go under Plugins and activate the plugin

== Screenshots ==
1. Widget setup for orientation and theme
2. Dark theme implementation
3. Default theme with vertical orientation

== Frequently Asked Questions ==
= What does the Muslimsalat and google API does for this plugin =
This APIs are used to get an approx location to display time and map.
