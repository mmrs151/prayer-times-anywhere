<?php

class PrayerTimePrinter
{
	/** @var string */
	private $theme;

	public function printVerticalTime($responseBody)
	{
		$times = $responseBody->items[0];
		$table = "";
		$table .= 
		"
		<table class='timetable " .$this->theme. "'>
		 <tr><th colspan='7' class='title'> Prayer times for " .current_time("jS F, Y"). " near <a href='https://www.google.co.uk/maps/place/" .$responseBody->city. "' target='_new'>".$responseBody->city."</a></th></tr>
		 <tr>
			 <th> Fajr </th>
			 <th> Shurooq </th>
			 <th> Dhuhr </th>
			 <th> Asr </th>
			 <th> Maghrib </th>
			 <th> Isha </th>
		 </tr>
		 <tr>
		 <td> " .$times->fajr. "</td>
		 <td> " .$times->shurooq. "</td>
		 <td> " .$times->dhuhr. "</td>
		 <td> " .$times->asr. "</td>
		 <td> " .$times->maghrib. "</td>
		 <td> " .$times->isha. "</td>
		 </tr>
		</table>
		";

		return $table;
	}

	public function printHorizontalTime($responseBody)
	{
		$times = $responseBody->items[0];
		$table = "";
		$table .= 
		"
		<table class='timetable " .$this->theme. "'>
		 <tr><th colspan='2' class='title'> Prayer times for " .current_time("jS F, Y"). " </th></tr>
		 <tr><th> Fajr </th><td> " .$times->fajr. "</td></tr>
		 <tr><th> Shurooq </th><td> " .$times->shurooq. "</td></tr>
		 <tr><th> Dhuhr </th><td> " .$times->dhuhr. "</td></tr>
		 <tr><th> Asr </th><td> " .$times->asr. "</td></tr>
		 <tr><th> Maghrib </th><td> " .$times->maghrib. "</td></tr>
		 <tr><th> Isha </th><td> " .$times->isha. "</td></tr>
		 <tr><td colspan='2' class='title'>Near <a href='https://www.google.co.uk/maps/place/" .$responseBody->city. "' target='_new'>".$responseBody->city."</a></td></tr>
		 </table>
		";

		return $table;
	}

	/**
	 * @param string $value
	 */
	public function setTheme($value)
	{
		$this->theme = $value;
	}
}


// object(stdClass)#14 (7) 
// { ["date_for"]=> string(9) "2015-8-30" 
// ["fajr"]=> string(7) "4:01 am" 
// ["shurooq"]=> string(7) "6:02 am" 
// ["dhuhr"]=> string(7) "1:01 pm" 
// ["asr"]=> string(7) "4:48 pm" 
// ["maghrib"]=> string(7) "8:00 pm" 
// ["isha"]=> string(7) "9:53 pm" 
// }