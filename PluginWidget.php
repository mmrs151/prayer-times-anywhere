<?php
/*
Plugin Name: Prayer times anywhere
Version: 1.0
Plugin URI: https://wordpress.org/plugins/prayer-times-anywhere/
Description: Show prayer start time based on your current location
Author: mmrs151
Author URI: http://mmrs151.tumblr.com/
*/

require_once('PrayerTimePrinter.php');


class PluginWidget extends WP_Widget
{
    /** @var PrayerTimePrinter */
    private $printer;  

    public function __construct()
    {
        $this->add_stylesheet();
        $this->printer = new PrayerTimePrinter();

        $widget_details = array(
            'className' => 'PluginWidget',
            'description' => 'Display prayer start time based on your current location'
        );
        parent::__construct('PrayerTimesAnywhere', 'Prayer times anywhere', $widget_details);
    }

    public function form($instance)
    {
        ?>
        <div xmlns="http://www.w3.org/1999/html">
            <span></br></br>
            <input
                type="checkbox"
                name="<?php echo $this->get_field_name( 'choice' ); ?>"
                value="vertical"
                <?php if($instance["choice"] === 'vertical'){ echo 'checked="checked"'; } ?>
            />Display prayer time vertically</br></br>
            Select Theme
                <select name="<?php echo $this->get_field_name( 'prayerWidgetTheme' ); ?>">
                    <option value="" <?php if($instance["prayerWidgetTheme"] === ''){ echo 'selected="selected"'; } ?>>Default</option>
                    <option value="noBorder" <?php if($instance["prayerWidgetTheme"] === 'noBorder'){ echo 'selected="selected"'; } ?>>No Border</option>
                    <option value="dark" <?php if($instance["prayerWidgetTheme"] === 'dark'){ echo 'selected="selected"'; } ?>>Dark</option>
                </select></br></br>                
            </span>
        </div>

        <div class='mfc-text'>
        </div>

        <?php

        echo $args['after_widget'];
        echo "</br><a href='http://edgwareict.org.uk/' target='_blank'>Support EICT</a></br></br>";
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        if (! empty($instance['prayerWidgetTheme'])) {
            $this->printer->setTheme($instance['prayerWidgetTheme']);
        }        

        $response = wp_remote_get( 'http://muslimsalat.com/daily.json?key=777a6497421fd5e13e5642bf7358fdda ', array( 'timeout' => 9) );

        if( is_array($response) ) {
            $header = $response['headers']; // array of http header lines
            $body = json_decode($response['body']); // use the content

            if ($instance['choice'] === 'vertical') {
                echo $this->printer->printVerticalTime($body);
            } else {
                echo $this->printer->printHorizontalTime($body);
            }

        } else {
            echo "<h4 class='red'>Connection Timeout, please contact <a href='mailto:mmrs151@gmail.com'>The Developer</a></h4>";
        }

        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }

    public function add_stylesheet() {
          wp_register_style( 'prayer-time-style', plugins_url('styles.css', __FILE__) );
          wp_enqueue_style( 'prayer-time-style' );
    }    
}

add_action('widgets_init', 'init_plugin_widget');
function init_plugin_widget()
{
    register_widget('PluginWidget');
}
